﻿using System;
using System.Linq;
using Pico_Placa_Predictor.Commands;
using Pico_Placa_Predictor.Data;

namespace Pico_Placa_Predictor.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private PredecirCommand predecirCommand;

        public PredecirCommand PredecirCommand
        {
            get { return predecirCommand; }
        }
        
        public string Placa
        {
            get;
            set;
        }

        private string _mensaje;
        public string Mensaje
        {
            get { return _mensaje;}
            set { _mensaje = value; OnPropertyChanged(); }
        }

        private readonly HorarioData _horarios;

        public FechaViewModel FechaVm { get; set; }

        public MainViewModel()
        {
            Mensaje = "Los resultados aparecen aquí:";
            predecirCommand = new PredecirCommand(this);
            _horarios = new HorarioData();
            Placa = "Ingresar Placa (PPP1234)";
            FechaVm = new FechaViewModel();
        }

        internal void Predecir()
        {
            int ultimoDigito;
            if (ValidarPlaca(ultimoDigito: out ultimoDigito))
            {
                if (PuedeCircular(ultimoDigito))
                {
                    this.Mensaje = "Circular sin problema";
                    return;
                }

                this.Mensaje = "No puede circular";
            }
        }

        private string GetPlacaNumber()
        {
            var number = this.Placa.Substring(this.Placa.Length - 1);
            return number;
        }

        private bool PuedeCircular(int ultimoDigito)
        {
            var dow = this.FechaVm.Fecha.DayOfWeek;
            var hora = this.FechaVm.Hora.Hour;
            var min = this.FechaVm.Hora.Minute;

            var horaSeleccionada = new TimeSpan(hora, min, 0);
            var horario = _horarios.Horarios.FirstOrDefault(h => h.Dia == (int)dow
                                                                 && (h.Placa1 == ultimoDigito || h.Placa2 == ultimoDigito)
                                                                 && ((horaSeleccionada >= h.InicioMatutino && horaSeleccionada <= h.FinMatutino) ||
                                                                     (horaSeleccionada >= h.InicioVespertino && horaSeleccionada <= h.FinVespertino)));
            if (horario == null)
                return true;

            return false;
        }

        private bool ValidarPlaca(out int ultimoDigito)
        {
            var number = GetPlacaNumber();
            
            if (!int.TryParse(number, out ultimoDigito))
            {
                this.Mensaje = "No se puede procesar la Placa ingresada";
                return false;
            }

            if (this.Placa.Length != 7)
            {
                this.Mensaje = "Placa Incorrecta";
                return false;
            }

            return true;
        }
    }
}
