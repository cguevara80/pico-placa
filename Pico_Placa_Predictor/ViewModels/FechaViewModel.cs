﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pico_Placa_Predictor.ViewModels
{
    public class FechaViewModel : ViewModelBase
    {
        public TimeSpan Interval
        {
            get;
            set;
        }

        public DateTime Fecha
        {
            get;
            set;
        }

        public DateTime Hora
        {
            get;
            set;
        }


        public FechaViewModel()
        {
            Fecha = DateTime.Today.Date;
            Interval = new TimeSpan(0, 0, 30, 0);
            Hora = DateTime.Now;
        }
    }
}
