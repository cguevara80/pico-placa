﻿using System;
using System.Globalization;

namespace Pico_Placa_Predictor.Model
{
    public class Horario
    {
        public int Dia { get; set; }
        public TimeSpan InicioMatutino { get; set; }
        public TimeSpan FinMatutino { get; set; }
        public TimeSpan InicioVespertino { get; set; }
        public TimeSpan FinVespertino { get; set; }

        public int Placa1 { get; set; }
        public int Placa2 { get; set; }

        public Horario(int i)
        {
            Dia = i;
            InicioMatutino = new TimeSpan(0,7,0,0);
            InicioVespertino = new TimeSpan(0, 16, 0, 0);
            FinMatutino = new TimeSpan(0,9,30,0);
            FinVespertino = new TimeSpan(0,19,30,0);

            SetPlacas(i);
        }

        private void SetPlacas(int dia)
        {
            switch (dia)
            {
                case 1:
                    Placa1 = 1;
                    Placa2 = 2;
                    break;
                case 2:
                    Placa1 = 3;
                    Placa2 = 4;
                    break;
                case 3:
                    Placa1 = 5;
                    Placa2 = 6;
                    break;
                case 4:
                    Placa1 = 7;
                    Placa2 = 8;
                    break;
                case 5:
                    Placa1 = 9;
                    Placa2 = 0;
                    break;
                default:
                    Placa1 = -1;
                    Placa2 = -2;
                    break;
            }
        }
    }
}
