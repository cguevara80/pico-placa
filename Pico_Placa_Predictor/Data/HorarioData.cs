﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pico_Placa_Predictor.Model;

namespace Pico_Placa_Predictor.Data
{
    public class HorarioData
    {
        public List<Horario> Horarios { get; set; }

        public HorarioData()
        {
            Horarios = new List<Horario>();
            LoadHorarios();
        }

        private void LoadHorarios()
        {
            for (int i = 0; i <= 6; i++)
            {
                var h = new Horario(i);
                Horarios.Add(h);
            }
        }
    }
}
