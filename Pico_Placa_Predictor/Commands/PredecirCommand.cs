﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using Pico_Placa_Predictor.ViewModels;

namespace Pico_Placa_Predictor.Commands
{
    public class PredecirCommand : ICommand
    {
        private MainViewModel _model;

        public PredecirCommand(MainViewModel vm)
        {
            _model = vm;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _model.Predecir();
        }

        public event EventHandler CanExecuteChanged;
    }
}
